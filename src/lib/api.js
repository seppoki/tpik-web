export const languages = [
    { label: 'english', api: 'eng' },
    { label: 'finnish', api: 'fin' },
    { label: 'swedish', api: 'swe' }
];
export const orders = [
    { label: 'match', api: 'rank' },
    { label: 'date', api: 'id' }
];
export const sources = [
    'Duunitori',
    'Kuntarekry',
    'Linkedin',
    'Monster',
    'Rekrytointi',
    'TE-palvelut',
    'Tiitus',
    'Valtiolle'
];
