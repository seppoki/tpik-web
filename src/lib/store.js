import { browser } from '$app/environment'
import { writable } from 'svelte/store'

const initial = browser ? window.localStorage.getItem('terms') : "";
export const terms = writable(initial || "");

terms.subscribe((v) => {
    if (browser) window.localStorage.setItem('terms', v);
});
